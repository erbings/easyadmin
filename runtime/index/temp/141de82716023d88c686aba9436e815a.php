<?php /*a:1:{s:61:"D:\phpstudy_pro\WWW\easyadmin\app\index\view\index\index.html";i:1656991921;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo sysconfig('site','site_ico'); ?>" type="image/x-icon">
    <title><?php echo sysconfig('site','site_name'); ?></title>
    <script type="text/javascript" src="/static/plugs/jquery-3.4.1/jquery-3.4.1.min.js"></script>
    <style type="text/css">
    *{padding: 0;margin: 0;}
    body{width:100%;height:100vh;background: url('/static/admin/images/index-bg.png') no-repeat;display: flex;justify-content: center;align-items: center}
    .main{width: 4.5rem;height:1.013rem;background-color: #fff;border-radius: 15px;padding-top: .281rem;}
    .main .title{font-size: .18rem;text-align: center;font-weight: 600;color: #333;}
    .main .box{display: flex;font-size: .1rem;color: #333;justify-content: space-around;margin:.168rem auto 0;width: 2.8rem;}
    .main .policeBox{display: flex;align-items:center;}
    .main .policeBox img{margin-right: .01rem;}
    .main .support{font-size: .1rem;text-align: center;color: #333;margin-top: .112rem;}
    .main .support a{color: #333;text-decoration: none;}
    </style>
</head>
<body>
    <div class="main">
        <div class="title">备案信息</div>
        <div class="box">
            <?php if(!empty($site_police_beian)): ?>
            <div class="policeBox"><img src="/static/admin/images/police.png" width="18" height="20"><?php echo htmlentities($site_police_beian); ?></div>
            <div>|</div>
            <?php endif; ?>
            <div><?php echo htmlentities($site_beian); ?></div>
        </div>
        <div class="support">技术支持：<a href="http://www.srok.cn/" target="_blank">圣融科技</a></div>
    </div>
    <script>
    $(function(){
        document.documentElement.style.fontSize = document.documentElement.clientWidth / 10.8 + "px";
    })
    </script>
</body>
</html>