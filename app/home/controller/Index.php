<?php
declare (strict_types = 1);

namespace app\Home\controller;
use hg\apidoc\annotation as Apidoc;
use app\common\controller\HomeController;
use think\facade\Db;


class Index extends HomeController
{
    public function index(){
        $this->assign([
            'site_beian' => sysconfig('site','site_beian'),
            'site_police_beian' => sysconfig('site','site_police_beian')
        ]);
        return $this->fetch();
    }

    
}