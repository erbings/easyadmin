<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller\system;


use app\admin\model\SystemBanner;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;
use think\facade\Db;

/**
 * @ControllerAnnotation(title="轮播图管理")
 * Class Banner
 * @package app\admin\controller\system
 */
class Banner extends AdminController{
    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new SystemBanner();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            $filed_arr = [
                'cate_name' => 'c.id'
            ];
            list($page, $limit, $where) = $this->buildTableParames([],$filed_arr);
            $count = Db::name('system_banner')->alias('b')
                ->leftjoin('system_banner_cate c ','b.cate_id = c.id')
                ->where($where)
                ->count();
            $list = Db::name('system_banner')->alias('b')
                ->leftjoin('system_banner_cate c ','b.cate_id = c.id')
                ->where($where)
                ->field('b.*,c.cate_name')
                ->page($page, $limit)
                ->order('list desc')
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }
}