<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller\system;


use app\admin\model\SystemUploadfile;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\facade\Db;
use think\facade\Env;
use think\App;

/**
 * @ControllerAnnotation(title="上传文件管理")
 * Class Uploadfile
 * @package app\admin\controller\system
 */
class Uploadfile extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new SystemUploadfile();
    }
    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            list($page, $limit, $where) = $this->buildTableParames();
            if(session('admin.upload') == 0){
                $where[] = ['admin_id','=',session('admin.id')];
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {
        $row = $this->model->where('id', $id)->find();
        $row->isEmpty() && $this->error('数据不存在');
        try {
            if($row['upload_type'] == 'local'){
                $urlArr = explode('/',$row['url']);
                $real_path = realpath(ROOT_PATH . 'public/' . $urlArr['3'] . '/' . $urlArr['4'] . '/' . $urlArr['5']);
                if (is_file($real_path) && !unlink($real_path)) {
                    $this->error('删除失败');
                }
            }
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }

    /**
     * @NodeAnotation(title="更新域名")
     */
    public function updateDomain(){
        if ($this->request->isAjax()) {
            $domain = $this->request->param('domain');
            if(empty($domain)) $this->error('缺少domain');
            $tables = Db::query("show tables");
            $database = 'Tables_in_'.Env::get('database.database');
            foreach ($tables as $table) {
                $columns = Db::query('SHOW FULL COLUMNS FROM '.$table[$database]);
                foreach ($columns as $column) {
                    if($column['Collation'] == 'utf8_general_ci'){
                        Db::query("update ".$table[$database]." set `".$column['Field']."` = REPLACE(`".$column['Field']."`,'".$domain."','".$_SERVER['HTTP_HOST']."')");
                    }
                }
            }
            $this->success('更新成功');
        }
        return $this->fetch();
    }
}