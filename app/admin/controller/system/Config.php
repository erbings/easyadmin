<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller\system;


use app\admin\model\SystemConfig;
use app\admin\service\TriggerService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * Class Config
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="系统配置管理")
 */
class Config extends AdminController
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new SystemConfig();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        $apiclient_cert = file_exists('cert/apiclient_cert.pem') ? file_get_contents('cert/apiclient_cert.pem') : '';
        $apiclient_key = file_exists('cert/apiclient_key.pem') ? file_get_contents('cert/apiclient_key.pem') : '';
        $certificate = file_exists('cert/cert.pem') ? file_get_contents('cert/cert.pem') : '';
        $this->assign('apiclient_cert', $apiclient_cert);
        $this->assign('certificate', $certificate);
        $this->assign('apiclient_key', $apiclient_key);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="保存")
     */
    public function save()
    {
        $post = $this->request->post();
        if(!empty($post['login_type'])){
            $post['login_type'] = implode(',',$post['login_type']);
        }
        try {
            foreach ($post as $key => $val) {
                $this->model
                    ->where('name', $key)
                    ->update([
                        'value' => $val,
                    ]);
            }
            if(!is_dir('cert')){
                mkdir('cert', 0755);
            }
            if(!empty($post['apiclient_cert'])){
                file_put_contents('cert/apiclient_cert.pem',$post['apiclient_cert']);
            }
            if(!empty($post['apiclient_key'])){
                file_put_contents('cert/apiclient_key.pem',$post['apiclient_key']);
            }
            TriggerService::updateMenu();
            TriggerService::updateSysconfig();
        } catch (\Exception $e) {
            $this->error('保存失败');
        }
        $this->success('保存成功');
    }

    public function update(){
        $this->updateCertificates();
        $this->success('更新成功');
    }
}