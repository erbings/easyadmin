<?php
declare (strict_types = 1);

namespace app\srapi\controller;
use hg\apidoc\annotation as Apidoc;
use app\common\controller\SrapiController;
use think\facade\Db;

/**
 * @Apidoc\Title("首页")
 * @Apidoc\Group("Home")
 */
class Home extends SrapiController
{
    /**
     * @Apidoc\Title("轮播图")
     * @Apidoc\Method("GET")
     * @Apidoc\Tag("sr")
     * @Apidoc\Param("cate_id", desc="分类")
     * @Apidoc\Returned("title", type="int", desc="标题")
     * @Apidoc\Returned("image", type="int", desc="图")
     * @Apidoc\Returned("url", type="int", desc="链接")
     */
    public function banner(){
        $data = $this->request->param();
        $where = [];
        if(!empty($data['cate_id'])){
            $where[] = ['cate_id','=',$data['cate_id']];
        }
        $list = Db::name('system_banner')->where($where)->order('list desc')->select();
        $this->success('', $list);
    }

    /**
     * @Apidoc\Title("是否交付")
     * @Apidoc\Method("GET")
     * @Apidoc\Tag("sr")
     * @Apidoc\Returned("state", type="int", desc="1未交付2已交付")
     * @Apidoc\Returned("image", type="int", desc="提示图片")
     */
    public function check(){
        $this->success('',['state'=>1, 'image'=>'https://api.rjaaa.com/common/images/tc_add_wap.png']);
    }
}