增加  
CURD命令创建的js增加默认的新增编辑删除按钮，控制器增加Db引用  
腾讯云CDN域名设置  
buildTableParames函数增加字段名转换  
echarts.js图表更新  
支持列表按搜索条件下载  
支持静态资源缓存加载，调试模式下页面对应js强制刷新  
支持搜索动态下拉{field: 'username', minWidth: 80, title: '后台用户', search: 'selectAjax', url: 'system.admin/index', selectFields: 'id,username'},  
支持搜索动态联动下拉{field: 'cate_name', title: '所属分类', search: 'selectLink', url: 'dishes/storeCate/store_id/', selectFields: 'id,cate_name', listener:'store_name'},
支持范围数值搜索{field: 'id', width: 80, title: 'ID', search: 'rangeInt', min:0, max: 10},  
隐藏表格右上角导出打印  public\static\plugs\layui-v2.5.6\layui.all.js注释
增加一键替换库中图片域名/admin/system.Uploadfile/updateDomain?domain=1289.com
  
修复  
修复上传弹出层在小窗口页面的弹出异常  
修复日志分页BUG  
修复七牛上传URL错误  
