define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.banner/index',
        add_url: 'system.banner/add',
        edit_url: 'system.banner/edit',
        delete_url: 'system.banner/delete',
        modify_url: 'system.banner/modify',
        export_url: 'system.banner/export',
        password_url: 'system.banner/password',
    };

    var Controller = {

        index: function () {

            ea.table.render({
                init: init,
                toolbar: ['refresh', 'add', 'delete'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},
                    {field: 'cate_name',  title: '分类', search: 'selectAjax', url: 'system.banner.cate/index', selectFields: 'id,cate_name'},
                    {field: 'title',  title: '标题'},
                    {field: 'image', width: 200, title: '图片', search: false, templet: ea.table.image},
                    {field: 'url',  title: '链接'},
                    {field: 'list', width: 80, title: '排序', edit: 'text'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});